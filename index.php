<?php
const ALLOWED_PAGE_IDS = [1, 2];

require_once 'data.php';
require_once 'functions.php';

$currentPageId = 1;

if (isset($_GET['page'])) {
    if (in_array($_GET['page'], ALLOWED_PAGE_IDS)) {
        $currentPageId = $_GET['page'];
    }
}

?>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonym ous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
        crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    <style>
        img {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        ul {
            list-style-type: none;
            -webkit-padding-start: 0px;
        }

        ul li{
            display: inline;
        }

        .content img {
            width: 100%;
        }

        .gallery img {
            max-width: 300px;
            padding-right: 10px;
        }
    </style>
</head>

<body>
    <?php printMenu($currentPageId, $menu)?>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <section class="row content">
                    <?php printContent($content[$currentPageId]);?>
                </section>
                <section class="row gallery">
                    <?php printGallery($content[$currentPageId]['gallery']);?>
                </section>

            </div>
            <div class="col-md-3 news">
                <?php printNews($news);?>
            </div>
        </div>
    </div>
</body>

</html>