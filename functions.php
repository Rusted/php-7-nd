<?php declare (strict_types = 1);

function printContent(array $content): void
{
    echo "<h1>";
    echo $content['name'];
    echo "</h1>";

    echo '<img src="img/' . $content['image'] . '" />';
    echo '<div>';
    echo $content['text'];
    echo '</div>';
}

function printMenu(int $activePageId, array $menu): void
{
    echo '<nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="?page=1">Universitetas</a>
            </div>';

    echo '<div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">';
    foreach ($menu as $menuItem) {
        echo '<li' . ($activePageId == $menuItem['id'] ? ' class="active"' : '') . '><a href="?page=' . $menuItem['id'] . '">' . $menuItem['text'] . '</a></li>';
    }

    echo '</ul>
            </div>
        </div>
    </nav>';

}

function printGallery(array $gallery): void
{
    echo '<ul>';
    foreach ($gallery as $image) {
        echo '<li><img src="img/' . $image . '"/></li>';
    }

    echo '</ul>';
}

function printNews(array $news): void
{
    echo '<h2>Naujienos</h2>';
    foreach ($news as $newsDate => $newsItem) {
        echo '<article>';
        echo '<h3>' . $newsItem['name'] . '</h3>';
        echo '<h4>' . $newsDate . '</h4>';
        echo '<h4>' . $newsItem['description'] . '</h4>';
        echo $newsItem['text'];
        echo '</article>';
    }
}
